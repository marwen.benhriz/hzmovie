import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Watched from './Watched';

describe('<Watched />', () => {
  test('it should mount', () => {
    render(<Watched />);
    
    const watched = screen.getByTestId('Watched');

    expect(watched).toBeInTheDocument();
  });
});