import React, { lazy, Suspense } from 'react';

const LazyWatched = lazy(() => import('./Watched'));

const Watched = props => (
  <Suspense fallback={null}>
    <LazyWatched {...props} />
  </Suspense>
);

export default Watched;
