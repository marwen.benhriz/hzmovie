import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import MovieControls from './MovieControls';

describe('<MovieControls />', () => {
  test('it should mount', () => {
    render(<MovieControls />);
    
    const movieControls = screen.getByTestId('MovieControls');

    expect(movieControls).toBeInTheDocument();
  });
});