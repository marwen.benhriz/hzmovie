import React, { lazy, Suspense } from 'react';

const LazyMovieControls = lazy(() => import('./MovieControls'));

const MovieControls = props => (
  <Suspense fallback={null}>
    <LazyMovieControls {...props} />
  </Suspense>
);

export default MovieControls;
