import React, { lazy, Suspense } from 'react';

const LazyAdd = lazy(() => import('./Add'));

const Add = props => (
  <Suspense fallback={null}>
    <LazyAdd {...props} />
  </Suspense>
);

export default Add;
