import React, { lazy, Suspense } from 'react';

const LazyResultCard = lazy(() => import('./ResultCard'));

const ResultCard = props => (
  <Suspense fallback={null}>
    <LazyResultCard {...props} />
  </Suspense>
);

export default ResultCard;
