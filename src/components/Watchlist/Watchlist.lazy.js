import React, { lazy, Suspense } from 'react';

const LazyWatchlist = lazy(() => import('./Watchlist'));

const Watchlist = props => (
  <Suspense fallback={null}>
    <LazyWatchlist {...props} />
  </Suspense>
);

export default Watchlist;
