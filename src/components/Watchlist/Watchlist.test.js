import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Watchlist from './Watchlist';

describe('<Watchlist />', () => {
  test('it should mount', () => {
    render(<Watchlist />);
    
    const watchlist = screen.getByTestId('Watchlist');

    expect(watchlist).toBeInTheDocument();
  });
});