import './App.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./lib/font-awesome/css/all.min.css";
import { Header } from "./components/Header/Header";
import { Watchlist } from "./components/Watchlist/Watchlist";
import { Watched } from "./components/Watched/Watched";
import { Add } from "./components/Add/Add";
import { GlobalProvider } from "./context/GlobalState";

//import { Home } from "./components/Home/Home";


function App() {
  return (
    <GlobalProvider>
      <Router>
        <Header />

        <Switch>
          <Route exact path="/">
            <Watchlist />
          </Route>
          <Route path="/add">
            <Add />
          </Route>
          <Route path="/watched">
            <Watched />
          </Route>
        </Switch>
      </Router>
    </GlobalProvider>
  );
}

export default App;
